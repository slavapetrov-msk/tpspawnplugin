package ru.msk.slavaPetrov.tpSpawnPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {

    Location spawnLoc;
    FileConfiguration config = getConfig();


    public void toConfig(String path, int value) {
        config.set(path, value);
        saveConfig();
    }
    public void toConfig(String path, String value) {
        config.set(path, value);
        saveConfig();
    }
    public void toConfig(String path, boolean value) {
        config.set(path, value);
        saveConfig();
    }
    public void toConfig(String path, Object value) {
        config.set(path, value);
        saveConfig();
    }


    public int getInt(String name) {
        return config.getInt(name);
    }
    public String getString(String name) {
        return config.getString(name);
    }
    public boolean getBool(String name) {
        return config.getBoolean(name);
    }



    @Override
    public void onEnable() {
        Bukkit.getServer().getPluginManager().registerEvents(this, this);

        String x = getString(Consts.SPAWN_LOCATION_X);
        String y = getString(Consts.SPAWN_LOCATION_Y);
        String z = getString(Consts.SPAWN_LOCATION_Z);


        if (x != null && y != null && z != null) {
            spawnLoc = new Location(
                    Bukkit.getWorld("world"),
                    Double.parseDouble(x),
                    Double.parseDouble(y),
                    Double.parseDouble(z)
            );
            spawnLoc.setPitch(getInt(Consts.SPAWN_LOCATION_PITCH));
            spawnLoc.setYaw(getInt(Consts.SPAWN_LOCATION_YAW));

        }

        getLogger().info(ChatColor.BLUE + "; " + y + ", " + z);
    }

    public boolean onCommand(CommandSender s, Command cmd, String label, String args[]) { // переменная команд
        if(cmd.getName().equalsIgnoreCase(Consts.SPAWN_COMMAND)) {
            Player player = Bukkit.getPlayer(s.getName());
            spawnLoc = player.getLocation();

            int x = (int) spawnLoc.getBlockX();
            int y = (int) spawnLoc.getBlockY();
            int z = (int) spawnLoc.getBlockZ();
            int pitch = (int) spawnLoc.getPitch();
            int yaw = (int) spawnLoc.getYaw();

            toConfig(Consts.SPAWN_LOCATION_X, x + 0.5);
            toConfig(Consts.SPAWN_LOCATION_Y, y);
            toConfig(Consts.SPAWN_LOCATION_Z, z + 0.5);
            toConfig(Consts.SPAWN_LOCATION_PITCH, pitch);
            toConfig(Consts.SPAWN_LOCATION_YAW, yaw);


            player.sendMessage(ChatColor.GREEN + "Спавн успешно установлен на\n x=" + x + "\ny=" + y + "\nz=" + z + "\npitch=" + pitch + "yaw=" + yaw);
            return true;
        }
        return false;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        p.teleport(spawnLoc);
    }

}
