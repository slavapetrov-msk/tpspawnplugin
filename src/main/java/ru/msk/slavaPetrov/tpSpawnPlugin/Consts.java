package ru.msk.slavaPetrov.tpSpawnPlugin;

public class Consts {
    public static String SPAWN_LOCATION_X = "x";
    public static String SPAWN_LOCATION_Y = "y";
    public static String SPAWN_LOCATION_Z = "z";

    public static String SPAWN_LOCATION_PITCH = "pitch";
    public static String SPAWN_LOCATION_YAW = "yaw";


    public static String SPAWN_COMMAND = "settpspawn";
}
